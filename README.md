
Desenvolvimento das Pastas do Projeto:
1.	Criado Maven Project:
a.	Eu criei por conta de unir: build do processo, das dependências e frameworks.
b.	Com as pastas com padrões brasileiras “br.com. consiste.estagio.provaimc"(group – grupo) /consisteprojectestagioimc(artifact -artefato))
c.	A possibilidade de usar OpenCSv devido a escrever, ler, serializar, desserializar, e, portanto, incluído nas dependências pox.xml.
2.	Criação de Pacotes:
a.	Foi criado 3 pacotes (Package) – modem.entidade/modem.serviços/modem.programa com objetivo de separar as funcionalidades deixando mais próximo de mundo real , ou seja, orientado a objetos ou Java. Lang.
3.	 Criação das Classes:
a.	 O pacote Modem.entidade = Classe Person (Pessoa) / Classe CalculatorIMc (cálculo do Imc)
b.	O pacote Modem.serviços = Classe PersonExtrator (extrair e ler a lista de pessoas com validação) / Classe PersonExporter (exportar para o arquivo “adrianampdelgado.txt as pessoas, IMC e formatações solicitadas)
c.	O pacote Modem,program = Classe Application (onde encontramos o main).

4.	Criação do arquivo. txt :
a.	Foi criando um arquivo chamado adriana_maria_pereira_delgado.txt  para apresentação da lista de pessoas formatado e respectivo IMC.

